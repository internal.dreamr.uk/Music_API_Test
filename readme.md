

# Music API Test

We would like you to create a small REST API for a Music streaming service that could be easily consumed by a mobile application. It can be as complicated or as simple as you want, but you should want to impress us.

##### The API should:

- be written in Laravel 5.3
- demonstrate the use of multiple HTTP request methods
- return all data as JSON
- contain seeders for all tables


This task is to test the structure, style and patterns of your code so you have complete freedom when it comes to what endpoints to create and what data return. 

#### Good Luck!


--------------------------------


###### Please clone this project and create a new branch, when you are done we will add you add as guest so you can push your example to the repo.
